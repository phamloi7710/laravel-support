<?php

    namespace LoiPham\LaravelSupport;

    use Illuminate\Support\ServiceProvider;
    use Illuminate\Routing\Router;

    class SupportServiceProvider extends ServiceProvider
    {
        public function __construct($app)
        {
            parent::__construct($app);
        }

        public function boot(Router $router)
        {

        }

        public function register()
        {

        }
    }
